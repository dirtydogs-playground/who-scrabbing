from selenium import webdriver
from selenium.webdriver.chrome.options import Options


DRIVER_PATH = './chromedriver'
options = Options()
# options.headless = True
options.add_argument("--window-size=1920,1200")

driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)
driver.get("https://search.bvsalud.org/gim/?output=site&lang=en&from=1&sort=&format=summary&count=20&fb=&page=1&filter[db][]=AIM&index=tw&q=")
# print(driver.page_source)

xpath = '//div[@class="box1"][1]'

BookName = driver.find_element_by_xpath(xpath+"//div[@class='textArt']//div[@class='titleArt']//a")
print(BookName.text)

Bookauthers = driver.find_element_by_xpath(xpath+"//div[@class='textArt']//div[@class='author']")
Authers = [x for x in Bookauthers.find_elements_by_tag_name("a")]
print(len(Authers))
for Auther in Authers:
    print(Auther.text) 

BookRefYear = driver.find_element_by_xpath(xpath+"//div[@class='textArt']//div[@class='reference']//em")
print(BookRefYear.text)

Bookid = driver.find_element_by_xpath(xpath+"//div[@class='textArt']//div[@class='dataArticle']")
print(Bookid.text)