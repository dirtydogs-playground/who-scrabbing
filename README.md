# Scrabby Demo

Its build to show the skill set with python

## Description

its a project to show the skill set with organizations,. in scrabbing, not intended to stealing any data! 
also this project will give basic idea to start with scrabing and class usage with scrabing

## Getting Started

### Dependencies

* Python (-inbuild with system)
* Chromedriver (Driver has to be installed with system)
* selenium (Testing tool)
* csv

### Installing

all the executables are indipendant as of now! each one intented to do saparate operation! 
once you installed the chrome driver the following line in all file need to be updated with the chromedriver path

* DRIVER_PATH = 

### Executing program

* Just to get a single block of data use basic.py
```
python basic.py
```
which will give you the book specific details on the console! 

* To get a single page data use who.py
```
python who.py
```
which will give you the who_page1.csv has all 20 datas what scrabbed from the first page
to get the 2nd page data! open up the who.py and change the URL of the page and redo the same to get next page data! 

## Help

Any advise for common problems or issues. please write to us [DirtyDogs](email:jilabaji@gmail.com)

