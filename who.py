import csv
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

DRIVER_PATH = './chromedriver'
options = Options()
options.headless = True
options.add_argument("--window-size=1920,1200")

driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)
driver.get("https://search.bvsalud.org/gim/?output=site&lang=en&from=1&sort=&format=summary&count=20&fb=&page=1&filter[db][]=AIM&index=tw&q=")
# print(driver.page_source)

# BookClass Declaration 
class Book:
    def __init__(self, domPath):
        self.domPath = domPath
        self.name = ''
        self.AuthersCount = ''
        self.Authers = []
        self.refYear = ''
        self.id = ''
    
    def __BookName(self): 
        BookName = driver.find_element_by_xpath(self.domPath+"//div[@class='textArt']//div[@class='titleArt']//a")
        self.name = BookName.text

    def __BookAuthers(self): 
        Bookauthers = driver.find_element_by_xpath(self.domPath+"//div[@class='textArt']//div[@class='author']")
        Authers = [x for x in Bookauthers.find_elements_by_tag_name("a")]
        self.AuthersCount = len(Authers)
        
        for Auther in Authers:
            self.Authers.append(Auther.text)
            # print(Auther.text) 

    def __refYear(self): 
        BookRefYear = driver.find_element_by_xpath(self.domPath+"//div[@class='textArt']//div[@class='reference']//em")
        self.refYear = BookRefYear.text

    def __id(self): 
        Bookid = driver.find_element_by_xpath(xpath+"//div[@class='textArt']//div[@class='dataArticle']")
        self.id = Bookid.text

    def __repr__(self):  
        return "Bookname:% s And Bookid:% s" % (self.name, self.id)

    def ParseData(self):
        # Bookname parcing
        self.__BookName()
        self.__BookAuthers()
        self.__refYear()
        self.__id()

    def get_list(self):
        return [
            self.name,
            self.AuthersCount,
            self.Authers,
            self.refYear,
            self.id,
        ]

BooksData = []

# To hit the next page not implemented yet
# driver.find_element_by_xpath("//ul[@class='pagination justify-content-center']//li[@class='page-item'][4]").click()

xpath = '//div[@id="main_container"]'
BooksDom = driver.find_element_by_xpath(xpath)
Booksarray = [x for x in BooksDom.find_elements_by_tag_name("div.box1")]
# print(len(Booksarray))
i=0
for Books in Booksarray:
    i += 1
    bookpath = xpath+'//div[@class="box1"]['+str(i)+']'
    
    indbook = Book(bookpath)
    indbook.ParseData()
    BooksData.append(indbook.get_list())

# Export data
## name of csv file  
filename = "Who_page1.csv"
## writing to csv file  
with open(filename, 'w') as csvfile:  
    # creating a csv writer object  
    csvwriter = csv.writer(csvfile)  

    # writing the data rows  
    csvwriter.writerows(BooksData) 